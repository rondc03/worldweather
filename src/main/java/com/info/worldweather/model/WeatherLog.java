package com.info.worldweather.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "WeatherLog", description = "Data model for Weather Information Result")
@JsonPropertyOrder({"id", "responseId", "location","actualWeather","temperature","dtimeInserted"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WeatherLog {
	
	 @Id
	 @NotNull
	 @Size(min = 1, max = 5)
	 private String id;

	 @NotNull
	 @Size(min = 0, max = 50)
	 private String responseId;
	 
	 @NotNull
	 @Size(min = 0, max = 50)
	 private String location;
	 
	 @NotNull
	 @Size(min = 0, max = 255)
	 private String actualWeather;
	 
	 @NotNull
	 @Size(min = 0, max = 50)
	 private String temperature;
	 
	 @NotNull
	 @Size(min = 0, max = 255)
	 private String dtimeInserted;
	 
	 // Getter Methods
	 @JsonProperty(value = "id", required = true)
	 @ApiModelProperty(position = 1, required = true, dataType = "long", example = "123", notes = "Unique auto number")
	 public String getId() {
	      return id;
	 }
	 
	 @JsonProperty(value = "responseId", required = true)
	 @ApiModelProperty(position = 2, required = true, dataType = "String", example = "12345", notes = "Unique guId")
	 public String getResponseId() {
	      return responseId;
	 }
	 
	 @JsonProperty(value = "location", required = true)
	 @ApiModelProperty(position = 3, required = true, dataType = "String", example = "London", notes = "Location")
	 public String getLocation() {
	      return location;
	 }
	 
	 @JsonProperty(value = "actualWeather", required = true)
	 @ApiModelProperty(position = 4, required = true, dataType = "String", example = "overcast clouds", notes = "Description of Actual Weather")
	 public String getActualWeather() {
	      return actualWeather;
	 }
	 
	 @JsonProperty(value = "temperature", required = true)
	 @ApiModelProperty(position = 5, required = true, dataType = "String", example = "275.88", notes = "Temperature")
	 public String getTemperature() {
	      return temperature;
	 }
	 
	 @JsonProperty(value = "dtimeInserted", required = true)
	 @ApiModelProperty(position = 6, required = true, dataType = "String", example = "1548947970", notes = "Timestamp Record Inserted")
	 public String getDtimeInserted() {
	      return dtimeInserted;
	 }
	 
	 
	 //Setter Methods
	 public void setId(String id) {
	      this.id = id;
	 }
	 
	 public void setResponseId(String responseId) {
	      this.responseId = responseId;
	 }
	 
	 public void setLocation(String location) {
	      this.location = location;
	 }
	 
	 public void setActualWeather(String actualWeather) {
	      this.actualWeather = actualWeather;
	 }
	 
	 public void setTemperature(String temperature) {
	      this.temperature = temperature;
	 }
	 
	 public void setDtimeInserted(String dtimeInserted) {
	      this.dtimeInserted = dtimeInserted;
	 }
}
