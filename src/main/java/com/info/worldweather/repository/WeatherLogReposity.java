package com.info.worldweather.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
//import org.springframework.security.access.prepost.PreAuthorize;

import com.info.worldweather.model.WeatherLog;

//@PreAuthorize("hasRole('ROLE_USER')")
@RepositoryRestResource(collectionResourceRel = "weatherLog", path = "weatherLog")
public interface WeatherLogReposity extends MongoRepository<WeatherLog, String> {

	//List<WeatherLog> save(List<WeatherLog> response);

}
