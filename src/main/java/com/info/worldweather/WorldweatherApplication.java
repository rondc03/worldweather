package com.info.worldweather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorldweatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorldweatherApplication.class, args);
	}

}

