FROM java:8
VOLUME /tmp
ADD target/worldweather-0.0.1-SNAPSHOT*.jar worldweather-0.0.1-SNAPSHOT.jar
RUN sh -c 'touch worldweather-0.0.1-SNAPSHOT.jar'
ENV JAVA_OPTS="-Xmx256m -Xms128m"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /worldweather-0.0.1-SNAPSHOT.jar" ]

## Pull base image
#FROM java:8-sdk

## Maintainer
MAINTAINER "ronel <rondc03@yahoo.com>"

## Copy to images tomcat path
ADD target/worldweather-0.0.1-SNAPSHOT.jar D:/apps/apache-tomcat-10.0.6/webapps/